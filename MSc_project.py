#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import nltk
import string
import json
import math
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from nltk.probability import FreqDist


#Setting up Data into Dataframes
path = '/Users/yashmehta/Documents/MSc_project/Code/test.json'
df = pd.read_json(path, lines=True, orient='records')

product_data = df['description']
title_data = df['title']

#Appending tile dataframe into description dataframe
product_data.append(title_data)

# Function for removing HTML tags from the data
def remove_html(text):
    test_list = []
    for i in text:
        soup_text = BeautifulSoup(i, 'lxml').text
        test_list.append(soup_text)
    return test_list


#Function for removing punctuations from the data
def remove_punctuation(text):
    no_punctuation = "".join([line for line in text if line not in string.punctuation])
    return no_punctuation


#Function for removing stopwords from the data
def remove_stopwords(text):
    stop_words = [w for w in text if w not in stopwords.words('english')]
    return stop_words

#Removing HTML tags from the data
product_data = product_data.apply(lambda x: remove_html(x))

#Removing punctuations from the data
product_data = product_data.apply(lambda x: remove_punctuation(x))

#Tokenizing the data
tokenizer = RegexpTokenizer(r'\w+')
product_data = product_data.apply(lambda x: tokenizer.tokenize(x.lower()))
print("Product data tokenized", product_data)

#Removing stopwords from the data
product_data = product_data.apply(lambda x: remove_stopwords(x))
print("Product data without stop_words", product_data)

#Function for lemmatizing the words from the data
lemmatizer = WordNetLemmatizer()
def word_lemmatizer(text):
    lemmatized_text = [lemmatizer.lemmatize(i) for i in text]
    return lemmatized_text

product_data = product_data.apply(lambda x: word_lemmatizer(x))
print("Product data Lemmas\n", product_data)

#Creating Ethical keywords list
ethical_words = ['natural','bio','eco-friendly','fairtrade','sustainable','non-toxic','nature','low-carbon footprint','parabens','zero waste']

#Fucntion to check for the presence of ethical keywords in the data
def get_ethicalwords(text):
    for i in range(len(product_data)):
        for j in range(len(product_data[i])):
            new_list = [w for w in text if w in ethical_words]
        break
    return new_list

new_data = product_data.apply(lambda x: get_ethicalwords(x))
print("Product data with Ethical Keywords", new_data)


#Generating Green Profile of a product
rating = 0
print("Green Profile is a product rating out of 5")
for i in range(len(new_data)):
        for j in range(len(new_data[i])):
            new_data[i] = list(dict.fromkeys(new_data[i]))
            print("Ethical Consumption Terminologies in the product:",new_data[i])
            rating = ((len(new_data[i])/ len(ethical_words))*10)
            print("Green Profile of the product:",math.ceil(rating/2))
            print("\n")




